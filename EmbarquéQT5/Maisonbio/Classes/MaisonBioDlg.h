
#ifndef _MAISONBIODLG_H
#define _MAISONBIODLG_H

// Includes qt
#include <QMainWindow>

// Includes application
#include "ui_MaisonBioDlg.h"
#include "BusCAN.h"
#include "basedonnees.h"

class MaisonBioDlg:public QMainWindow, private Ui_MaisonBioDlg
{
	Q_OBJECT
	bool opencan,openport;
	BusCAN *mcan;

public :

    MaisonBioDlg();
    ~MaisonBioDlg();

protected :

private :
 //   Mcp2515 *mon_mcp ;
  //  ReceptionCAN *mon_thread;
    BaseDonnees *ma_base;
    QString path;

public slots:
	void EcrireIHM();
	void LectureBit();

};


#endif  // _MAISONBIODLG_H

