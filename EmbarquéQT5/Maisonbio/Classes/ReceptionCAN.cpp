/*
 * ReceptionCAN.cpp
 *
 *  Created on: 8 avr. 2015
 *      Author: Maxime. A
 */

#include "ReceptionCAN.h"
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>

ReceptionCAN::ReceptionCAN()
{
	this->start();
	nonlue = true;
	fileopen = false;
	interrupt=0;
//	emit versInterrupt();

}

ReceptionCAN::~ReceptionCAN()
{

}


void ReceptionCAN::run()
{
	char val;
	while(1)
	{
		 if( (interrupt=open("/sys/class/gpio/gpio39_pb7/value", O_RDWR)) < 0 )
		 {

			fileopen=false;
		 }
		 read(interrupt,&val,1);
		 close(interrupt);

		 if ((val=='0')&&(nonlue))
		 {
			 nonlue=false;
			 emit versInterrupt();
		 }
	}
}

void ReceptionCAN::Raz_Int()
{
	nonlue=true;
}
