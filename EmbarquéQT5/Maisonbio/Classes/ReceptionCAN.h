/*
 * ReceptionCAN.h
 *
 *  Created on: 8 avr. 2015
 *      Author: Maxime. A
 */

#ifndef RECEPTIONCAN_H_
#define RECEPTIONCAN_H_
#include "REGS2515.h"
#include <qthread.h>
#include "Mcp2515.h"
#include <iostream>

class ReceptionCAN : public QThread{
	Q_OBJECT
public:
	ReceptionCAN();
	virtual ~ReceptionCAN();
	void Raz_Int();
protected:
	void run();
private:
	int interrupt;
	bool nonlue;
	bool fileopen;

signals:
	void versInterrupt();

};

#endif /* RECEPTIONCAN_H_ */
