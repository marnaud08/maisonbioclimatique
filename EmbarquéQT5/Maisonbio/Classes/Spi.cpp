/*
 * Spi.cpp
 *
 *  Created on: 31 déc. 2014
 *      Author: michel
 */

#include "Spi.h"
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <sys/ioctl.h>
#include <linux/types.h>
#include <linux/spi/spidev.h>

#define device "/dev/spidev2.0"



Spi::Spi() {
	int ret;
	unsigned int mode,bits,speed;
	mode=0;
	fdspi = open(device, O_RDWR);
	if (fdspi < 0)
		fileopen=false;
	else fileopen=true;

	/*
	 * spi mode
	 */
	ret = ioctl(fdspi, SPI_IOC_WR_MODE, &mode);
//	if (ret == -1)
//		ui->Message->setText("can't set spi mode");

}

Spi::~Spi() {

	close(fdspi);
}


void Spi::Ecrire(unsigned char *command,int longueur){
    int  r = write(fdspi, command, longueur);
}

int Spi::Lire(unsigned char *command,int longueur){
    unsigned char value;
    int  r = write(fdspi, command, longueur);
   // usleep(delay);
    for(long int i=0;i<100000;i++);
      // the read is always one step behind the selected input
     r = read(fdspi, &value, 1);
     return value;
}

int Spi::LireU(unsigned char *command,int longueur){
    unsigned char value;
//    int  r = write(fdspi, command, longueur);
//	  usleep(delay);
//    for(long int i=0;i<100000;i++);
      // the read is always one step behind the selected input
     int r = read(fdspi, command, longueur);
     return command[longueur-1];
}




bool Spi::getOpen(){
	return fileopen;
}
