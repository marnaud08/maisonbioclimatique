/*
 * Mcp2515.h
 *
 *  Created on: 1 janv. 2015
 *      Author: michel
 */

#ifndef MCP2515_H_
#define MCP2515_H_
#include "REGS2515.h"
#include "Spi.h"
#include "battement.h"


class Mcp2515
{
private:
	Spi *mspi;
	Battement *mbat;
	int cs;
	char *s_0;
	char *s_1;
	bool fileopen;
public:
	Mcp2515();
	virtual ~Mcp2515();
	void Reset();
	void ecrireReg(unsigned char addr, unsigned char value);
	unsigned char LireByte(unsigned char addr);
	unsigned char LireUByte(unsigned char addr);
	void envoyerMsg(unsigned int CANdata);
	unsigned char RTS(unsigned char);
	void delayms();
	void configEnvoiTX0(void);
	void messageRecu(int *identifiant,unsigned char *data );
	int init();
	void setNormalMode();
	void ecrireByte(char address, char data, char mask);
	unsigned char lireStatus();
	unsigned short lireCAN(unsigned char* donnees);
	void Recu();

};

#endif /* MCP2515_H_ */
