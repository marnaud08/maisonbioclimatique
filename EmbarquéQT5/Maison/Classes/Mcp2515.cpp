/*
 * Mcp2515.cpp
 *
 *  Created on: 1 janv. 2015
 *      Author: michel
 */

#include "Mcp2515.h"
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <qthread.h>

#define CAN_500kbps 1       //BRP setting in CNF1
#define CAN_250kbps 3       //Based on 16 MHz Fosc
#define CAN_125kbps 7       //

Mcp2515::Mcp2515() {

	//long int i;
	//char msg[10];
    mspi= new Spi();
    mbat=new Battement();
    fileopen=true;
    s_0=new char[2];
    s_0[0] = '0';s_0[1]=0;
    s_1=new char[2];
    s_1[0] = '1';s_1[1]=0;
    if( (cs=open("/sys/class/gpio/gpio35_pb3/value", O_RDWR)) < 0 )
    {
    	fileopen=false;
    }
    init();

}

Mcp2515::~Mcp2515()
{
	// TODO Auto-generated destructor stub
    close(cs);
}

int Mcp2515::init()
{
	unsigned char dummy=0;
    this->Reset();
    //tempo 1ms;
    delayms();
 //   QThread::msleep(1);
    this->Reset();
    delayms();
    //tempo 1ms;
    //Clear masks to RX all messages
    this->ecrireReg(RXM0SIDH,0x00);
    this->ecrireReg(RXM0SIDL,0x00);

    //Clear filter... really only concerned to clear EXIDE bit
    this->ecrireReg(RXF0SIDL,0x00);

    //Set CNF1
    this->ecrireReg(CNF1,CAN_250kbps);

    //Set CNF2
    this->ecrireReg(CNF2,0x80 | PHSEG1_3TQ | PRSEG_1TQ);

     //Set CNF3
    this->ecrireReg(CNF3, PHSEG2_3TQ);

     //Set TXB0 DLC and Data for a "Write Register" Input Message to the MCP25020
    this->ecrireReg(TXB0SIDH,0xA0);    //Set TXB0 SIDH
    this->ecrireReg(TXB0SIDL,0x00);    //Set TXB0 SIDL
    this->ecrireReg(TXB0DLC,DLC_3);    //Set DLC = 3 bytes
    this->ecrireReg(TXB0D0,0x1E);      //D0 = Addr = 0x1E
    this->ecrireReg(TXB0D1,0x10);      //D1 = Mask = 0x10

     //Set TXB1 DLC and Data for a "READ I/O IRM"
    this->ecrireReg(TXB1SIDH,0x50);    //Set TXB0 SIDH
    this->ecrireReg(TXB1SIDL,0x00);    //Set TXB0 SIDL
    this->ecrireReg(TXB1DLC,0x40 | DLC_8);    //Set DLC = 3 bytes and RTR bit

     //Interrupt on RXB0 - CANINTE
    this->ecrireReg(CANINTE,0x01);    //
   // this->ecrireReg(CANINTE,0x00);    //

     //Set NORMAL mode
    this->ecrireReg(CANCTRL,REQOP_NORMAL | CLKOUT_ENABLED);

     //Verify device entered Normal mode
/*     dummy = LireByte(CANSTAT);
     if (OPMODE_NORMAL != (dummy && 0xE0))
         this->ecrireReg(CANCTRL,REQOP_NORMAL | CLKOUT_ENABLED);
         */
    setNormalMode();
 	// Disable filter and mask on buffer 0
 //	can_write_bits(RXB0CTRL, 0x60, 0x60);
    ecrireByte(RXB0CTRL, 0x60, 0x60);
	return dummy;
}


void Mcp2515::ecrireByte(char address, char mask, char data){
    /*write(cs, s_0, strlen(s_0));
    mspi->Ecrire(CAN_BIT_MODIFY);
    mspi->Ecrire(address);
    mspi->Ecrire(mask);
    mspi->Ecrire(data);
    write(cs, s_1, strlen(s_1));*/
	unsigned char commande[4];
	commande[0]=CAN_BIT_MODIFY;
	commande[1]=address;
	commande[2]=mask;
	commande[3]=data;
	write(cs, s_0, strlen(s_0));
	mspi->Ecrire(commande,4);
	write(cs, s_1, strlen(s_1));
}



////////////////////////////////////////////////////////////
// Set the controller in normal mode
////////////////////////////////////////////////////////////
void Mcp2515::setNormalMode(){

	ecrireByte(CANCTRL, 0x00, 0xE0);
	while ((LireByte(CANSTAT) & 0xE0) != 0);

	return;
}


void Mcp2515::Reset(){

	//mspi->Ecrire(CAN_RESET);
	write(cs, s_0, strlen(s_0));
	unsigned char val=CAN_RESET;
	mspi->Ecrire(&val,1);
    write(cs, s_1, strlen(s_1));
}

void Mcp2515::ecrireReg(unsigned char addr, unsigned char value){
	/*
	write(cs, s_0, strlen(s_0));
    mspi->Ecrire(addr);
    mspi->Ecrire(value);
    */
	unsigned char commande[3];
	commande[0]=CAN_WRITE;
	commande[1]=addr;
	commande[2]=value;
	write(cs, s_0, strlen(s_0));
	mspi->Ecrire(commande,3);
    write(cs, s_1, strlen(s_1));

}

unsigned char Mcp2515::LireUByte(unsigned char addr)
{
    unsigned char tempdata;
    unsigned char commande[3];
  	commande[0]=CAN_READ;
  	commande[1]=addr;
  	commande[2]=0;
  	write(cs, s_0, strlen(s_0));
  	tempdata=mspi->LireU(commande,3);
  	write(cs, s_1, strlen(s_1));
    return tempdata;
}



unsigned char Mcp2515::LireByte(unsigned char addr)
{
    unsigned char tempdata;
    /*
    mspi->Ecrire(CAN_READ);
    mspi->Ecrire(addr);
    tempdata=mspi->Lire();
    */
  	unsigned char commande[3];
  	commande[0]=CAN_READ;
  	commande[1]=addr;
  	commande[2]=0;
  	write(cs, s_0, strlen(s_0));
  	tempdata=mspi->Lire(commande,2);
  	write(cs, s_1, strlen(s_1));
    return tempdata;
}

unsigned char Mcp2515::lireStatus(){
  unsigned char tempdata;
  /*
  mspi->Ecrire(CAN_RD_STATUS);
  tempdata=mspi->Lire();
  mspi->Lire();
  write(cs, s_1, strlen(s_1));
  */
  	unsigned char commande[2];
  	commande[0]=CAN_RD_STATUS;
  	commande[1]=0;
  	write(cs, s_0, strlen(s_0));
  	tempdata=mspi->Lire(commande,2);
  	write(cs, s_1, strlen(s_1));
  return tempdata;

}

/*
void configEnvoiTX0(void)
{
//val++;
 SPIByteWrite(TXB0SIDH,0xe0);    //Set TXB0 SIDH
 SPIByteWrite(TXB0SIDL,0x00);    //Set TXB0 SIDL
 SPIByteWrite(TXB0DLC,DLC_3);    //Set DLC = 3 bytes
 SPIByteWrite(TXB0D0,SPIByteRead(RXB0D0));
 SPIByteWrite(TXB0D1,SPIByteRead(RXB0D1));
 SPIByteWrite(TXB0D2,SPIByteRead(RXB0D2));
 RTS(CAN_RTS_TXB0);
}



	  val = SPIByteRead(RXB0D0);
SPIByteWrite(CANINTF, 0x00);  //Clear RX0IF; Actually clear all of CANINTF
gRXFlag = 0;  //Clear gRXFlag bit
configEnvoiTX0();
*/

void Mcp2515::configEnvoiTX0(void){
	this->ecrireReg(TXB0SIDH,0xE0);
	this->ecrireReg(TXB0SIDL,0x00);
	this->ecrireReg(TXB0DLC,DLC_3);
	this->ecrireReg(TXB0D0,LireByte(RXB0D0));
	this->ecrireReg(TXB0D1,LireByte(RXB0D1));
	this->ecrireReg(TXB0D2,LireByte(RXB0D2));
	 RTS(CAN_RTS_TXB0);
}


void Mcp2515::messageRecu(int *identifiant,unsigned char *data ){
	data[0]=LireByte(RXB0D0);
	this->ecrireReg(CANINTF, 0x00);  //Clear RX0IF; Actually clear all of CANINTF
//	gRXFlag = 0;  //Clear gRXFlag bit
//	configEnvoiTX0();

}

void Mcp2515::delayms(){
	mbat->raztemps();
	while (mbat->liretemps()<10);
}


void Mcp2515::envoyerMsg(unsigned int CANData){
//    if(gSampleFlag)   //Check gSampleFlag bit
//    {
    	this->ecrireReg(TXB0D2,CANData);  //Write the value to data byte #3

         //SendMSG()
           RTS(CAN_RTS_TXB0);  //RTS msg - Input msg to 25020
//           RTS(CAN_RTS_TXB1);  //RTS msg - IRM message

//          gSampleFlag = 0;                  //Clear gSampleFlag bit

//    }
}
unsigned char Mcp2515::RTS(unsigned char buffer){
	  unsigned char tempdata,status=100;
	tempdata = lireStatus();// SPIByteRead(CAN_RD_STATUS);
	if(buffer & 0x01)	//Buffer 1
		if(tempdata & 0x04)	//Check TXREQ first
		{
  		delayms();
  		ecrireReg(TXB1CTRL, 0);			//Clear TXREQ (and everything else... not clean)
			while(status==((lireStatus()) & (0x04))); //Wait for TXREQ to clear
		}
	if(buffer & 0x02)	//Buffer 1
		if(tempdata & 0x10)	//Check TXREQ first
		{
  		delayms();
  		ecrireReg(TXB1CTRL, 0);			//Clear TXREQ (and everything else... not clean)
			while(status==((LireByte(CAN_RD_STATUS)) & (0x10))); //Wait for TXREQ to clear
		}
	if(buffer & 0x04)	//Buffer 2
		if(tempdata & 0x40)	//Check TXREQ first
		{
  		delayms();
  		ecrireReg(TXB2CTRL, 0);			//Clear TXREQ (and everything else... not clean)
			while(LireByte(CAN_RD_STATUS) & 0x40); //Wait for TXREQ to clear
		}

    write(cs, s_0, strlen(s_0));
	mspi->Ecrire(&buffer,1);
    write(cs, s_1, strlen(s_1));
    return buffer;
}

//Méthode de lecture de la trame CAN
unsigned short Mcp2515::lireCAN(unsigned char* donnees)
{
	char SIDH;
	char SIDL;

	unsigned short ID;

	SIDH=LireByte(RXB0SIDH);
	SIDL=LireByte(RXB0SIDL);

	ID=(SIDH*8)+(SIDL/32);

	donnees[0]=0;
	donnees[0]=LireByte(RXB0D0);
	donnees[1]=LireByte(RXB0D1);
	donnees[2]=LireByte(RXB0D2);
	donnees[3]=LireByte(RXB0D3);
	donnees[4]=LireByte(RXB0D4);
	donnees[5]=LireByte(RXB0D5);
	donnees[6]=LireByte(RXB0D6);
	donnees[7]=LireByte(RXB0D7);

	/*donnees[0]=0;
	donnees[1]=1;
	donnees[2]=2;
	donnees[3]=3;
	donnees[4]=4;
	donnees[5]=5;
	donnees[6]=6;
	donnees[7]=7;*/

//	MessageRecu(donnees);

	return ID;
}

void Mcp2515::Recu()
{
	this->ecrireReg(CANINTF, 0x00);
}

/*
//-----------------------------------------------------------------------------
//  RTS(buffer)
//	buffer = CAN_RTS_TXBn; where 'n' = 0, 1, 2
//	buffer = CAN_RTS; followed by | 0 - 7 (e.g., CAN_RTS | 7)
//-----------------------------------------------------------------------------
void RTS(unsigned char buffer)
{
  unsigned char tempdata;
	tempdata = SPIByteRead(CAN_RD_STATUS);

	if(buffer & 0x01)	//Buffer 0
		if(tempdata & 0x04)	//Check TXREQ first
		{
			Delay_ms(1);
			SPIByteWrite(TXB0CTRL, 0);			//Clear TXREQ (and everything else... not clean)
			while(SPIByteRead(CAN_RD_STATUS) & 0x04); //Wait for TXREQ to clear
	  }
	if(buffer & 0x02)	//Buffer 1
		if(tempdata & 0x10)	//Check TXREQ first
		{
  		Delay_ms(1);
  		SPIByteWrite(TXB1CTRL, 0);			//Clear TXREQ (and everything else... not clean)
			while(SPIByteRead(CAN_RD_STATUS) & 0x10); //Wait for TXREQ to clear
		}
	if(buffer & 0x04)	//Buffer 2
		if(tempdata & 0x40)	//Check TXREQ first
		{
  		Delay_ms(1);
  		SPIByteWrite(TXB2CTRL, 0);			//Clear TXREQ (and everything else... not clean)
			while(SPIByteRead(CAN_RD_STATUS) & 0x40); //Wait for TXREQ to clear
		}

	CS = LOW;;
	WriteSPI(buffer);
	CS = HIGH;;
}
*/


