#ifndef BASEDONNEES_H
#define BASEDONNEES_H
#include <QtSql>
#include <QObject>
#include <QtDebug>

class BaseDonnees : public QObject
{
    Q_OBJECT
    bool bvalid;
    QSqlDatabase db;
    bool baseok;

public:
    explicit BaseDonnees( QObject *parent = 0);
    ~BaseDonnees();
    bool connecter(QString path);
    bool getValid();
    bool creerTable();
    bool inserer(int ValE, int ValI, int ValT, int ValC, QString s);
    bool afficher();
    bool modifVal();
    bool supLigne(int index);
    bool supTable();
    bool fermer();
    bool fichier();
bool inserer();
	int MaJ_Energie(QString req, QString pan);

signals:

public slots:

};

#endif // BASEDONNEES_H
