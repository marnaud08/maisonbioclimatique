#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include "MaisonBioDlg.h"
#include "Mcp2515.h"


MaisonBioDlg::MaisonBioDlg()
{
	path = "MyProjetOceanVital";
//	path = "identifiant";
	this->setupUi(this);
	opencan=false;
	openport=false;
	int i,j;
	char val;

	QByteArray devUSB,devACM;
	QString devusb;
	QVector<QString> dev;
    QProcess* process = new QProcess();
    QStringList sArgs;
    process->start("sh -c \"ls /dev/ttyU*\"");
    process->waitForFinished(-1);
  // qDebug() << "hello;" ;
 devUSB= process->readAllStandardOutput();
 i=0;
 while (i<devUSB.length()){
	 j=0;
	while(devUSB[i]!=0xa){
		val=devUSB[i];
		devusb+=val;
		i++;j++;
	}
	i++;
	dev.push_back(devusb);
 	devusb.clear();
}
 process->start("sh -c \"ls /dev/ttyA*\"");
 process->waitForFinished(-1);
// qDebug() << "hello;" ;
devUSB= process->readAllStandardOutput();
i=0;
while (i<devUSB.length()){
	 j=0;
	while(devUSB[i]!=0xa){
		val=devUSB[i];
		devusb+=val;
		i++;j++;
	}
	i++;
	dev.push_back(devusb);
	devusb.clear();
}


	//mon_mcp = new Mcp2515();
	//mon_thread=new ReceptionCAN();
	ma_base = new BaseDonnees(this);
	connect(this->pushButton, SIGNAL(clicked()), this, SLOT(EcrireIHM()));
	//connect(mon_thread, SIGNAL(versInterrupt()), this, SLOT(EcrireIHM()));
	ma_base->connecter(path);
	 if (!openport){
		mcan=new BusCAN(this);
	 	connect(mcan,SIGNAL(pourLire()),this,SLOT(EcrireIHM()));
	 	//connect(mcan,SIGNAL(confirm()),this,SLOT(onConfirm()));
		openport=true;
	  if (mcan->openCAN(&dev[0])){
		  this->lineEdit->setText("open");
		  qDebug()<<dev[0];
		  mcan->ouvrirCAN(250);
		  opencan=true;
	  }
	  else	this->lineEdit->setText("close");
	  }


}

MaisonBioDlg::~MaisonBioDlg()
{
	if (opencan)
		delete mcan;

}

void MaisonBioDlg::EcrireIHM()
{

	TMessage messagecan;
	//Attributs
	QString val;
	unsigned char data[6];
	float intensite;
	float tension;
	float puissance;
	float temperature;

	float intens;
	float tens;
	float temp;

	//Choix du panneau TEST
	//QString s = lineEdit->text();
	//int i = s.toInt();

	//Lecture de la trame CAN
	//unsigned short id=mon_mcp->lireCAN(data);
	messagecan=mcan->getMessage();
	qDebug()<<messagecan.id;
	short id=messagecan.id;
	bool val1;
	for (int i=0;i<8;i++){
		data[i]=messagecan.donnee[i].toInt(&val1,16);
	}
	//int i = 1;
	lineEdit->setText("val");
	//Implémentation des lineEdit en fonction du panenau
	switch (id){ //switch (i):test   switch (id):méthode réelle
	case 1:
		tension = ((data[0]*256)+(data[1]));
		tens=tension/10;
		val.setNum(tens);
		this->tensionLineEdit1->setText(val);

		intensite = ((data[2]*256)+(data[3]));
		intens = intensite/100;
		val.setNum(intens);
		this->intensitLineEdit_1->setText(val);

		puissance = ((data[4]*256)+(data[5]));
		val.setNum(puissance);
		this->energieLineEdit_1->setText(val);


		temperature = ((data[6]*256)+(data[7]));
		temp = temperature/10;
		temp -=  20;
		val.setNum(temp);
		this->temperatureLineEdit_1->setText(val);

		ma_base->inserer(puissance, intensite, tension, temperature, "INSERT INTO Panneau1 (DateAcqui, HeureAcqui, Energie, Intensite, Tension, Temperature) VALUES ( ?,?,?,?,?,?)");
		break;

	case 2:
		tension = ((data[0])*256+(data[1]));
		tens=tension/10;
		val.setNum(tens);
		this->tensionLineEdit_2->setText(val);

		intensite = ((data[2])*256+(data[3]));
		intens = intensite/100;
		val.setNum(intens);
		this->intensitLineEdit_2->setText(val);

		puissance = ((data[4])*256+(data[5]));
		val.setNum(puissance);
		this->energieLineEdit_2->setText(val);


		temperature = ((data[6])*256+(data[7]));
		temp = temperature/10;
		temp -=  20;
		val.setNum(temp);
		this->temperatureLineEdit_2->setText(val);

		ma_base->inserer(puissance, intensite, tension, temperature, "INSERT INTO Panneau2 (DateAcqui, HeureAcqui, Energie, Intensite, Tension, Temperature) VALUES ( ?,?,?,?,?,?)");

		break;

	case 3:
		tension = ((data[0])*256+(data[1]));
		tens=tension/10;
		val.setNum(tens);
		this->tensionLineEdit_3->setText(val);

		intensite = ((data[2])*256+(data[3]));
		intens = intensite/100;
		val.setNum(intens);
		this->intensitLineEdit_3->setText(val);

		puissance = ((data[4])*256+(data[5]));
		val.setNum(puissance);
		this->energieLineEdit_3->setText(val);


		temperature = ((data[6])*256+(data[7]));
		temp = temperature/10;
		temp -=  20;
		val.setNum(temp);
		this->temperatureLineEdit_3->setText(val);

		ma_base->inserer(puissance, intensite, tension, temperature, "INSERT INTO Panneau3 (DateAcqui, HeureAcqui, Energie, Intensite, Tension, Temperature) VALUES ( ?,?,?,?,?,?)");

		break;

	case 4:
		tension = ((data[0])*256+(data[1]));
		tens=tension/10;
		val.setNum(tens);
		this->tensionLineEdit_4->setText(val);

		intensite = ((data[2])*256+(data[3]));
		intens = intensite/100;
		val.setNum(intens);
		this->intensitLineEdit_4->setText(val);

		puissance = ((data[4])*256+(data[5]));
		val.setNum(puissance);
		this->energieLineEdit_4->setText(val);


		temperature = ((data[6])*256+(data[7]));
		temp = temperature/10;
		temp -=  20;
		val.setNum(temp);
		this->temperatureLineEdit_4->setText(val);

		ma_base->inserer(puissance, intensite, tension, temperature, "INSERT INTO Panneau4 (DateAcqui, HeureAcqui, Energie, Intensite, Tension, Temperature) VALUES ( ?,?,?,?,?,?)");

		break;

	case 5:
		tension = ((data[0])*256+(data[1]));
		tens=tension/10;
		val.setNum(tens);
		this->tensionLineEdit_5->setText(val);

		intensite = ((data[2])*256+(data[3]));
		intens = intensite/100;
		val.setNum(intens);
		this->intensitLineEdit_5->setText(val);

		puissance = ((data[4])*256+(data[5]));
		val.setNum(puissance);
		this->energieLineEdit_5->setText(val);


		temperature = ((data[6])*256+(data[7]));
		temp = temperature/10;
		temp -=  20;
		val.setNum(temp);
		this->temperatureLineEdit_5->setText(val);

		ma_base->inserer(puissance, intensite, tension, temperature, "INSERT INTO Panneau5 (DateAcqui, HeureAcqui, Energie, Intensite, Tension, Temperature) VALUES ( ?,?,?,?,?,?)");

		break;

	case 6:
		tension = ((data[0])*256+(data[1]));
		tens=tension/10;
		val.setNum(tens);
		this->tensionLineEdit_6->setText(val);

		intensite = ((data[2])*256+(data[3]));
		intens = intensite/100;
		val.setNum(intens);
		this->intensitLineEdit_6->setText(val);

		puissance = ((data[4])*256+(data[5]));
		val.setNum(puissance);
		this->energieLineEdit_6->setText(val);


		temperature = ((data[6])*256+(data[7]));
		temp = temperature/10;
		temp -=  20;
		val.setNum(temp);
		this->temperatureLineEdit_6->setText(val);

		ma_base->inserer(puissance, intensite, tension, temperature, "INSERT INTO Panneau6 (DateAcqui, HeureAcqui, Energie, Intensite, Tension, Temperature) VALUES ( ?,?,?,?,?,?)");

		break;

	case 7:
		tension = ((data[0])*256+(data[1]));
		tens=tension/10;
		val.setNum(tens);
		this->tensionLineEdit_7->setText(val);

		intensite = ((data[2])*256+(data[3]));
		intens = intensite/100;
		val.setNum(intens);
		this->intensitLineEdit_7->setText(val);

		puissance = ((data[4])*256+(data[5]));
		val.setNum(puissance);
		this->energieLineEdit_7->setText(val);


		temperature = ((data[6])*256+(data[7]));
		temp = temperature/10;
		temp -=  20;
		val.setNum(temp);
		this->temperatureLineEdit_7->setText(val);

		ma_base->inserer(puissance, intensite, tension, temperature, "INSERT INTO Panneau7 (DateAcqui, HeureAcqui, Energie, Intensite, Tension, Temperature) VALUES ( ?,?,?,?,?,?)");

		break;

	case 8:
		tension = ((data[0])*256+(data[1]));
		tens=tension/10;
		val.setNum(tens);
		this->tensionLineEdit_8->setText(val);

		intensite = ((data[2])*256+(data[3]));
		intens = intensite/100;
		val.setNum(intens);
		this->intensitLineEdit_8->setText(val);

		puissance = ((data[4])*256+(data[5]));
		val.setNum(puissance);
		this->energieLineEdit_8->setText(val);


		temperature = ((data[6])*256+(data[7]));
		temp = temperature/10;
		temp -=  20;
		val.setNum(temp);
		this->temperatureLineEdit_8->setText(val);

		ma_base->inserer(puissance, intensite, tension, temperature, "INSERT INTO Panneau8 (DateAcqui, HeureAcqui, Energie, Intensite, Tension, Temperature) VALUES ( ?,?,?,?,?,?)");

		break;

	case 9:
		tension = ((data[0])*256+(data[1]));
		tens=tension/10;
		val.setNum(tens);
		this->tensionLineEdit_9->setText(val);

		intensite = ((data[2])*256+(data[3]));
		intens = intensite/100;
		val.setNum(intens);
		this->intensitLineEdit_9->setText(val);

		puissance = ((data[4])*256+(data[5]));
		val.setNum(puissance);
		this->energieLineEdit_9->setText(val);


		temperature = ((data[6])*256+(data[7]));
		temp = temperature/10;
		temp -=  20;
		val.setNum(temp);
		this->temperatureLineEdit_9->setText(val);

		ma_base->inserer(puissance, intensite, tension, temperature, "INSERT INTO Panneau9 (DateAcqui, HeureAcqui, Energie, Intensite, Tension, Temperature) VALUES ( ?,?,?,?,?,?)");

		break;

	case 10:
		tension = ((data[0])*256+(data[1]));
		tens=tension/10;
		val.setNum(tens);
		this->tensionLineEdit_10->setText(val);

		intensite = ((data[2])*256+(data[3]));
		intens = intensite/100;
		val.setNum(intens);
		this->intensitLineEdit_10->setText(val);

		puissance = ((data[4])*256+(data[5]));
		val.setNum(puissance);
		this->energieLineEdit_10->setText(val);


		temperature = ((data[6])*256+(data[7]));
		temp = temperature/10;
		temp -=  20;
		val.setNum(temp);
		this->temperatureLineEdit_10->setText(val);

		ma_base->inserer(puissance, intensite, tension, temperature, "INSERT INTO Panneau10 (DateAcqui, HeureAcqui, Energie, Intensite, Tension, Temperature) VALUES ( ?,?,?,?,?,?)");

		break;

	case 11:
		tension = ((data[0])*256+(data[1]));
		tens=tension/10;
		val.setNum(tens);
		this->tensionLineEdit_11->setText(val);

		intensite = ((data[2])*256+(data[3]));
		intens = intensite/100;
		val.setNum(intens);
		this->intensitLineEdit_11->setText(val);

		puissance = ((data[4])*256+(data[5]));
		val.setNum(puissance);
		this->energieLineEdit_11->setText(val);


		temperature = ((data[6])*256+(data[7]));
		temp = temperature/10;
		temp -=  20;
		val.setNum(temp);
		this->temperatureLineEdit_11->setText(val);

		ma_base->inserer(puissance, intensite, tension, temperature, "INSERT INTO Panneau11 (DateAcqui, HeureAcqui, Energie, Intensite, Tension, Temperature) VALUES ( ?,?,?,?,?,?)");

		break;

	case 12:
		tension = ((data[0])*256+(data[1]));
		tens=tension/10;
		val.setNum(tens);
		this->tensionLineEdit_12->setText(val);

		intensite = ((data[2])*256+(data[3]));
		intens = intensite/100;
		val.setNum(intens);
		this->intensitLineEdit_12->setText(val);

		puissance = ((data[4])*256+(data[5]));
		val.setNum(puissance);
		this->energieLineEdit_12->setText(val);


		temperature = ((data[6])*256+(data[7]));
		temp = temperature/10;
		temp -=  20;
		val.setNum(temp);
		this->temperatureLineEdit_12->setText(val);

		ma_base->inserer(puissance, intensite, tension, temperature, "INSERT INTO Panneau12 (DateAcqui, HeureAcqui, Energie, Intensite, Tension, Temperature) VALUES ( ?,?,?,?,?,?)");

		break;

	case 13:
		tension = ((data[0])*256+(data[1]));
		tens=tension/10;
		val.setNum(tens);
		this->tensionLineEdit_13->setText(val);

		intensite = ((data[2])*256+(data[3]));
		intens = intensite/100;
		val.setNum(intens);
		this->intensitLineEdit_13->setText(val);

		puissance = ((data[4])*256+(data[5]));
		val.setNum(puissance);
		this->energieLineEdit_13->setText(val);


		temperature = ((data[6])*256+(data[7]));
		temp = temperature/10;
		temp -=  20;
		val.setNum(temp);
		this->temperatureLineEdit_13->setText(val);

		ma_base->inserer(puissance, intensite, tension, temperature, "INSERT INTO Panneau13 (DateAcqui, HeureAcqui, Energie, Intensite, Tension, Temperature) VALUES ( ?,?,?,?,?,?)");

		break;

	case 14:
		tension = ((data[0])*256+(data[1]));
		tens=tension/10;
		val.setNum(tens);
		this->tensionLineEdit_14->setText(val);

		intensite = ((data[2])*256+(data[3]));
		intens = intensite/100;
		val.setNum(intens);
		this->intensitLineEdit_14->setText(val);

		puissance = ((data[4])*256+(data[5]));
		val.setNum(puissance);
		this->energieLineEdit_14->setText(val);


		temperature = ((data[6])*256+(data[7]));
		temp = temperature/10;
		temp -=  20;
		val.setNum(temp);
		this->temperatureLineEdit_14->setText(val);

		ma_base->inserer(puissance, intensite, tension, temperature, "INSERT INTO Panneau14 (DateAcqui, HeureAcqui, Energie, Intensite, Tension, Temperature) VALUES ( ?,?,?,?,?,?)");

		break;

	case 15:
		tension = ((data[0])*256+(data[1]));
		tens=tension/10;
		val.setNum(tens);
		this->tensionLineEdit_15->setText(val);

		intensite = ((data[2])*256+(data[3]));
		intens = intensite/100;
		val.setNum(intens);
		this->intensitLineEdit_15->setText(val);

		puissance = ((data[4])*256+(data[5]));
		val.setNum(puissance);
		this->energieLineEdit_15->setText(val);


		temperature = ((data[6])*256+(data[7]));
		temp = temperature/10;
		temp -=  20;
		val.setNum(temp);
		this->temperatureLineEdit_15->setText(val);

		ma_base->inserer(puissance, intensite, tension, temperature, "INSERT INTO Panneau15 (DateAcqui, HeureAcqui, Energie, Intensite, Tension, Temperature) VALUES ( ?,?,?,?,?,?)");

		break;
	case 16:
		tension = ((data[0])*256+(data[1]));
		tens=tension/10;
		val.setNum(tens);
		this->tensionLineEdit_16->setText(val);

		intensite = ((data[2])*256+(data[3]));
		intens = intensite/100;
		val.setNum(intens);
		this->intensitLineEdit_16->setText(val);

		puissance = ((data[4])*256+(data[5]));
		val.setNum(puissance);
		this->energieLineEdit_16->setText(val);


		temperature = ((data[6])*256+(data[7]));
		temp = temperature/10;
		temp -=  20;
		val.setNum(temp);
		this->temperatureLineEdit_16->setText(val);

		ma_base->inserer(puissance, intensite, tension, temperature, "INSERT INTO Panneau16 (DateAcqui, HeureAcqui, Energie, Intensite, Tension, Temperature) VALUES ( ?,?,?,?,?,?)");

		break;
	}

//	mon_mcp->Recu();
//	mon_thread->Raz_Int();
}

void MaisonBioDlg::LectureBit()
{

}
