/*
 * Spi.h
 *
 *  Created on: 31 déc. 2014
 *      Author: michel
 */

#ifndef SPI_H_
#define SPI_H_
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>

class Spi {
    int fdspi;
    char *s_0;
    char *s_1;
    bool fileopen;
public:
	Spi();
	virtual ~Spi();
	void Ecrire(unsigned char *command,int longueur);
	int Lire(unsigned char *command,int longueur);
	int LireU(unsigned char *command,int longueur);
	bool getOpen();
};

#endif /* SPI_H_ */
