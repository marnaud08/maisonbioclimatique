<html>
<body>
<meta http-equiv="refresh" content="60;url=alerte.php?pan=<?php echo $_GET['pan']; ?>" />
<?php
	include ('inc/head.php');  		// inclusion de head.php
	include ('config/config.php'); 		// inclusion de config.php
	$idpanneau = $_GET['pan'];		// récupérer varible pan transmis par l'URL
	$heure = date("H:i:s");			// Récupérer la date du serveur
	//$heure = date("16:10:11");
	$date = date("Y-m-d");			// Récupérer l'heure du serveur
	//$date = date("2015-03-25");

?>
	<table id='table_info'>
<?php
	//Ecrire les info: numero du panneau, la date et l'heure du jour
	echo '
		<tr>
			<td>
				<h4><u>Panneau : '.$idpanneau.'</u></h4>
			</td>
			<td>
				<h4>Date : '.$date.'</h4>
			</td>
			<td>
				<h4>Heure : '.$heure.'</h4>
			</td>
		</tr>';
?>
</table>
<?php
	//switch case pour choisir la requete associé au choix de la page accueil.php
		switch ($idpanneau)
	{
		case 1:
			$sql = "SELECT * FROM Panneau1 order by id desc limit 1";
			break;
		case 2:
			$sql = "SELECT * FROM Panneau2 order by id desc limit 1";
			break;
		case 3:
			$sql = "SELECT * FROM Panneau3 order by id desc limit 1";
			break;
		case 4:
			$sql = "SELECT * FROM Panneau4 order by id desc limit 1";
			break;
		case 5:
			$sql = "SELECT * FROM Panneau5 order by id desc limit 1";
			break;
		case 6:
			$sql = "SELECT * FROM Panneau6 order by id desc limit 1";
			break;
		case 7:
			$sql = "SELECT * FROM Panneau7 order by id desc limit 1";
			break;
		case 8:
			$sql = "SELECT * FROM Panneau8 order by id desc limit 1";
			break;
		case 9:
			$sql = "SELECT * FROM Panneau9 order by id desc limit 1";
			break;
		case 10:
			$sql = "SELECT * FROM Panneau10 order by id desc limit 1";
			break;
		case 11:
			$sql = "SELECT * FROM Panneau11 order by id desc limit 1";
			break;
		case 12:
			$sql = "SELECT * FROM Panneau12 order by id desc limit 1";
			break;
		case 13:
			$sql = "SELECT * FROM Panneau13 order by id desc limit 1";
			break;
		case 14:
			$sql = "SELECT * FROM Panneau14 order by id desc limit 1";
			break;
		case 15:
			$sql = "SELECT * FROM Panneau15 order by id desc limit 1";
			break;
		case 16:
			$sql = "SELECT * FROM Panneau16 order by id desc limit 1";
			break;
	}
	$lecture = mysql_query($sql);
	if (!$lecture)
	{
		echo"Impossible d'exécuter la requete sql";
		exit;
	}
?>
<?php		$bdd = "SELECT * FROM Energie WHERE DateAcqui in ('".$date."')";
		$energie = mysql_query($bdd);
		$variable = mysql_fetch_assoc($energie);
		echo "Energie Produite aujourd'hui: ".$variable['Panneau'.$idpanneau.'']." KJ</br></br>";
?>

	<table class='tableau_historique'>
		<tr>
			<td><h4>Date</h4></td>
			<td><h4>Heure</h4></td>
			<td><h4>Energie</h4></td>
			<td><h4>Puissance</h4></td>
			<td><h4>Intensité</h4></td>
			<td><h4>Tension</h4></td>
			<td><h4>Température</h4></td>
		</tr>
		<?php
			include "traitement/conversions.php";
			$val = new conversions;
			$row = mysql_fetch_assoc($lecture);
			if (isset($row ['DateAcqui']) AND isset($row ['HeureAcqui']))
				//si la date et l'heure existe alors ecrire les valeurs de la bdd
			{
			$cal = $val->intensite($row ['Intensite']) * $val->tension($row ['Tension']);
			echo '
				<tr>
					<td>'.$row['DateAcqui'] .'</td>
					<td>'.$row['HeureAcqui'] .'</td>
					<td>'.$row['Energie'].' KJ</td>
					<td>'.$cal.' W </td>
					<td>'. $val->intensite($row['Intensite']).'  A</td>
					<td>'. $val->tension($row['Tension']).' V</td>
					<td>'. $val->temperature($row['Temperature']).' °C</td>
				</tr>
			';
			$DateEvenement = new DateTime($row['DateAcqui'], new DateTimezone("Europe/Paris"));
			$DateNow = new DateTime($date,new DateTimezone("Europe/Paris"));
			$diffdate = $DateNow->diff($DateEvenement);
			$heureEvenement = new DateTime($row['HeureAcqui'], new DateTimezone("Europe/Paris"));
                	$heureNow = new DateTime($heure,new DateTimezone("Europe/Paris"));
                	$diffheure = $heureNow->diff($heureEvenement);

			printf("Dernière acquisition:</br> %s Année %s Mois %s Jour  </br> %sH %sMin %sSec ",
			$diffdate->y, $diffdate->m,$diffdate->d,$diffheure->h,$diffheure->i,$diffheure->s);
			echo '</br>';
			}
			else
				//sinon affichier une erreur
			{
				echo "<p class='erreur'> Aucune données </p>";
			};
		?>
	</table>
</body>
</html>
