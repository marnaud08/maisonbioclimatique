 <?php
	include ("config/config.php");		//lien de config
?>
<html>
	<head>
		<?php
			include ("inc/head.php");	//lien du head
		?>
		<title>Fichier EXCEL</title>	<!-- titre de l'onglet -->
	</head>
	<body onload="init();">
		<?php
			include ("inc/menu.php");	//lien du menu
			include ("inc/header.php");	//Lien du header
		?>
		<div class="div_valeur">
			<h4><u>Fichier de sauvegarde en EXCEL:</u></h4>		<!-- Titre du rectangle blanc-->
			<div id="div_historique_form">
				</br>
				<?php
			        if(isset($_GET['var']))
        			{
               				$val=$_GET['var'];
                			if($val=1)
                			{
                        			echo '<p class="erreur">Interval de date incorrecte !</p>';
                			}
                			else
                			{
                			}
        			}
				?>
				<form name='testform' method='post' action='csv/sauvegarde.php' style='margin:0px;'>
					<h4 class="titre_form">Choisissez un panneau :</h4>
					<?php
	    				// Variable qui ajoutera l'attribut selected de la liste déroulante
	    				$selected = '';
	    				// Parcours du tableau
	    				echo '<select id ="menu_deroulant" name="panneau">',"\n";
	    				for($i=1; $i<=16; $i+=1)
	    				{
	    				 // Définir la valeur par défaut 
	    				 if($i == 1)
	    				  {
	    				    $selected = ' selected="selected"';
	    				  }
	    				  // Affichage de la ligne
	    				  echo "\t",'<option value="', $i ,'"', $selected ,'> Panneau ', $i ,'</option>',"\n";
	    				  // Remise à zéro de $selected
	    				  $selected='';
	    				}
	    				echo '</select>',"\n";
	    			?>
				 </br>

                                       <!-- FICHIER DE STYLE DU CALENDRIER -->
					<link rel='stylesheet' href='gnoocalendar.css' />
					<!-- FICHIER DE SCRIPT DU CALENDRIER -->
					<script type="text/javascript" src="gnoocalendar.js"></script>
					<!-- Début du srcipt -->
					<script type="text/javascript">
						<!--
						/******************************/
						self.defaultStatus = "GnooCalendar 1.4";
						/******************************/
						/* 
						* instanciation de l'objet
						*/
						var CL1 = new GnooCalendar("CL1", 0, 5 );
						var CL2 = new GnooCalendar("CL2", 0, 5 );
						/******************************/
						function init()
						{
							CL1.init("calend1", document.forms["testform"].elements["testestformield1"]);
							CL1.isDragable(true);

							CL2.init("calend2", document.forms["testform"].elements["testestformield2"]);
                                                        CL2.isDragable(true);
						}
						/******************************/
						//-->
						<?php $datedujour = date("Y-m-d");?>
					</script>
					<h4 class="titre_form">Choisissez une date date de début:</h4>
					<?php 
						// Encare de la date avec la date du jour en valeur auto
						echo "<input id='date1' type='text' name='testestformield1'  value='". $datedujour."' />";
					?>
					<!-- Bouton pour afficher le calendrier -->
					<input class='input_afficher' type='button' name='show1' onclick='CL1.show(); CL1.setTitle("Date de Debut"); CL1.setFormat("fr");' value='Afficher le calendrier' />
					<div id="calend1" style="position: absolute; top: 370px; left: 55%; width: 200px; height: 200px; z-index: 99; border: solid 0px #000000;visibility: hidden;">
						&nbsp;
					</div>
					<h4 class="titre_form">Choisissez une date de fin:</h4>
                                        <?php 
                                                // Encare de la date avec la date du jour en valeur auto
                                                echo "<input id='date' type='text' name='testestformield2'  value='". $datedujour."' />";
                                        ?>
                                        <!-- Bouton pour afficher le calendrier -->
                                        <input class='input_afficher' type='button' name='show2' onclick='CL2.show(); CL2.setTitle("Date de Fin"); CL2.setFormat("fr");' value='Afficher le calendrier' />
                                        <div id="calend2" style="position: absolute; top: 370px; left: 65%; width: 200px; height: 200px; z-index: 99; border: solid 0px #000000;visibility: hidden;">
                                                &nbsp;
                                        </div>


					<input id="input_Voir" type="submit" value="Voir"> 	<!-- Bouton voir -->
				</form>
			</div>
		</div>

	</body>
</html>
